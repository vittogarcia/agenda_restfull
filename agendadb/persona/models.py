from django.db import models
from model_utils.models import TimeStampedModel


class Hobby(TimeStampedModel):
    hobby = models.CharField('Hobby', max_length=50)

    class Meta:
        verbose_name = 'Hobby'
        verbose_name_plural = 'Hobbies'

    def __str__(self):
        return self.hobby

class Person(TimeStampedModel):
    full_name = models.CharField('Full Name', max_length=100)
    job = models.CharField('Job', max_length=50)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField('Phone', max_length=15, blank=True)
    hobbies = models.ManyToManyField(Hobby)

    class Meta:
        verbose_name = 'Person'
        verbose_name_plural = 'Persons'
    
    def __str__(self):
        return self.full_name

class Reunion(TimeStampedModel):
    fecha = models.DateField()
    hora = models.TimeField()
    asunto = models.CharField('Title', max_length=100)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Reunion'
        verbose_name_plural = 'Reunions'

    def __str__(self):
        return self.asunto
