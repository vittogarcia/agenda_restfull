from django.urls import path, re_path
from . import views

app_name = 'person_app'

urlpatterns = [
    path(
        'persons/',
        views.PersonListView.as_view(),
        name = 'persons'
    ),
    path(
        'api/persons/list/',
        views.PersonListAPIView.as_view(),
    ),
    path(
        'list/',
        views.PersonListViewT.as_view(),
        name = 'list'
    ),
    path(
        'api/persons/search/<kword>/',
        views.PersonSearchAPIView.as_view(),
    ),
    path(
        'api/person/create/',
        views.PersonCreateAPIView.as_view(),
    ),
    path(
        'api/person/detail/<pk>/',
        views.PersonDetailAPIView.as_view(),
    ),
    path(
        'api/person/delete/<pk>/',
        views.PersonDeleteAPIView.as_view(),
    ),
    path(
        'api/person/update/<pk>/',
        views.PersonUpdateAPIView.as_view(),
    ),
    path(
        'api/persons/',
        views.PersonAPIList.as_view(),
    ),
    path(
        'api/persons2/',
        views.PersonAPIList2.as_view(),
    ),    
]