from typing import List
from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.base import TemplateView
from .models import Person
# REST FRAMEWORKS
from .serializers import (
    PersonSerializer, 
    PSerializer,
    PSerializer2,
)
from rest_framework.generics import (
    ListAPIView, 
    CreateAPIView, 
    RetrieveAPIView,
    DestroyAPIView,
    RetrieveUpdateAPIView,
)


class PersonListView(ListView):
    template_name = 'persons.html'
    context_object_name = 'persons'

    def get_queryset(self):
        return Person.objects.all()

class PersonListAPIView(ListAPIView):
    serializer_class = PersonSerializer
    
    def get_queryset(self):
        return Person.objects.all()

class PersonSearchAPIView(ListAPIView):
    serializer_class = PersonSerializer

    def get_queryset(self):
        kword = self.kwargs['kword']
        return Person.objects.filter(full_name__icontains=kword)

class PersonListViewT(TemplateView):
    template_name = 'lista.html'

class PersonCreateAPIView(CreateAPIView):
    serializer_class = PersonSerializer

class PersonDetailAPIView(RetrieveAPIView):
    serializer_class = PersonSerializer
    queryset = Person.objects.filter()

class PersonDeleteAPIView(DestroyAPIView):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()

class PersonUpdateAPIView(RetrieveUpdateAPIView):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()

class PersonAPIList(ListAPIView):
    #### VISTA PARA INTERACTUAR CON SERIALIZADORES ####
    serializer_class = PSerializer

    def get_queryset(self):
        return Person.objects.all()

class PersonAPIList2(ListAPIView):
    #### VISTA PARA INTERACTUAR CON SERIALIZADORES ####
    serializer_class = PSerializer2

    def get_queryset(self):
        return Person.objects.all()