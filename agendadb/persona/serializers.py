from rest_framework import serializers
from .models import Person

class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('__all__')

class PSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    full_name = serializers.CharField()
    job = serializers.CharField()
    email = serializers.EmailField()
    phone = serializers.CharField()
    # Atributo no obligatorio y no esta en el modelo
    activo = serializers.BooleanField(required=False)

class PSerializer2(serializers.ModelSerializer):
    activo = serializers.BooleanField(default=False)
    class Meta:
        model = Person
        fields = ('__all__')